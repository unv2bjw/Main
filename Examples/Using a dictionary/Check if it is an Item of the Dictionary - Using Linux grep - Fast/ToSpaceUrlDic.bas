REM ToSpaceUrlDic.bas
REM Convert Windows Text File to Linux With Space and URL Encoding

sourceDic$ = "german.dic"
!^ german.dic is a plain Windows text file
!^ from sourceforge.net/projects/germandict
!^ over 1.9 million entries (including inflected forms)
dic$ = "germanSpaceURL.dic" %Characters outside ASCII code are encryptetd as URL code, 
!^ because they will be ignored in Android's shell implementation.
!^ In this case according to the ISO-8859-1 standard.
!^ Mostly also "UTF-8" is possible. But the character set has to be overall the same in the commands.
!^ The first character has to be a white space, because BSD grep is not fully/correct implemented.
shortDic$ = "shortGermanSpaceURL.dic" % For very short words like "in" and your own stuff

FILE.ROOT path$ % Basic!'s data path
t1 =TIME()
GRABFILE text$, "file://" + path$ + "/" + sourceDic$
? "Step 1"
SPLIT.ALL resultArray$[], text$, CHR$(13) + CHR$(10)
!^ File from Windows. Got a memory fault using a file without CR.
? "Step 2"
text$ = ""
ARRAY.LENGTH al, resultArray$[]
? "Step 3"
!WAKELOCK 5
BYTE.OPEN w, bftD,  "file://" + path$ + "/" + dic$
BYTE.OPEN w, bftS,  "file://" + path$ + "/" + shortDic$
deciAl = INT(al / 10)
lc = 1
FOR i = 1 TO al
 IF i = deciAl * lc THEN ? "Loop Counter "; int$(lc); "/10: "; deciAl * lc: ++lc
 text$ = " " + ENCODE$("URL", "ISO-8859-1", resultArray$[i]) + "\n"
 IF LEN(resultArray$[i]) < 4 %You can optimize, if you change maybe to higher values
  !* Store short words into "shortGermanDicSpaceURL.dic"
  BYTE.WRITE.BUFFER bftS, text$
 ELSE
  !* Store longer words into "germanSpaceURL.dic"
  BYTE.WRITE.BUFFER bftD, text$
 ENDIF
NEXT
BYTE.CLOSE bftD
BYTE.CLOSE bftM
!WAKELOCK 5
t2 =TIME()
PRINT "Encoding time: ", (t2-t1)/1000; " sec  "; (t2-t1)/1000/60; " min"
